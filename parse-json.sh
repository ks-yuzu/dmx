#! /bin/bash

for day in `seq -w 1 31`
do
	for page in `seq -w 1 9`
	do
		cat data/days/c-repo10${day}-p${page}.json | \
			jq -r '.items[] | [.full_name, .size, .owner.type, .homepage, .stargazers_count, .watchers_count, .has_issues, .has_downloads, .has_wiki, .has_pages, .forks_count, .mirror_url, .open_issues_count, .forks, .default_branch, .score] | @csv' > ./tmp/c-repo10${day}-p${page}.csv
	done
done

		
