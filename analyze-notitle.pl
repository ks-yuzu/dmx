#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';
use Data::Dumper;

use Statistics::Lite 'sum';
use Statistics::Lite 'mean';
use Statistics::Lite 'variance';
use Statistics::Lite 'mode';

if( $#ARGV == -1 )
{
	say "error : no arg";
	die;
}

##### loading #####
my $input_file  = $ARGV[0];
#my $output_file = "result.csv";

open(my $fin,  '<', $input_file)  or die "cannot open '$!'";
#open(my $fout, '>', $output_file) or die "cannot open '$!'";

my @lines = <$fin>;
my $content = join '', @lines;
my $all_length = length $content;

##### section1 #####
my @line_lengths;
my $indent_level = 0;
my @indent_levels;

for my $line (@lines)
{
	push @line_lengths, length($line) - 1;

	$indent_level++	if( $line =~ m/{/ );
	$indent_level-- if( $line =~ m/}/ );
	push @indent_levels, $indent_level;
}

say mean @line_lengths;
say variance @line_lengths;

say mean @indent_levels;
say variance @indent_levels;


##### section3 #####
my @comment_lengths;
my $num_comment_line = 0;

# 1 line comment
push @comment_lengths, length $1  while $content =~ m/\/\/(.*)/mg;
$num_comment_line += $content =~ s/\/\/(.*)//mg;

# multi line comment
while( $content =~ m/\/\*(.*?)\*\//sg )
{
	my $comment = $1;
	push @comment_lengths, length $comment;

	my $count = 1;	$count++ while $comment =~ m/\n/g; # no NL code -> 1 line
	$num_comment_line += $count;
}

$content =~ s/\/\*(.*?)\*\///sg;

# no blank and no comment
@line_lengths = ();
$indent_level = 0;
@indent_levels = ();

while( $content =~ m/(.+)\n/g)
{
	my $line = $1;
	push @line_lengths, length($1);

	$indent_level++	if( $line =~ m/{/ );
	$indent_level-- if( $line =~ m/}/ );
	push @indent_levels, $indent_level;
}

say mean @line_lengths;
say variance @line_lengths;

say mean @indent_levels;
say variance @indent_levels;


say mean @comment_lengths;
say variance @comment_lengths;

say ($all_length != 0 ? sum @comment_lengths / $all_length : "null");
say ($#lines > -1 ? $num_comment_line / ($#lines + 1) : "null");


##### section2 #####
## count brace and new line code
my $lbrace = 0;
my $rbrace = 0;
my $nl_before_lbrace = 0;
my $nl_after_lbrace = 0;
my $nl_before_rbrace = 0;
my $nl_after_rbrace = 0;

while( $content =~ m/(?<lbrace>.{.)/sg )
{
	$lbrace++;
	$nl_before_lbrace++  if(substr($+{lbrace}, 0, 1) eq "\n");
	$nl_after_lbrace++   if(substr($+{lbrace}, 2, 1) eq "\n");
}

while( $content =~ m/(?<rbrace>.}.)/sg )
{
	$rbrace++;
	$nl_before_rbrace++  if(substr($+{rbrace}, 0, 1) eq "\n");
	$nl_after_rbrace++   if(substr($+{rbrace}, 2, 1) eq "\n");
}

say ($lbrace != 0 ? $nl_before_lbrace / $lbrace : "null");
say ($lbrace != 0 ? $nl_after_lbrace / $lbrace  : "null");
say ($rbrace != 0 ? $nl_before_rbrace / $rbrace : "null");
say ($rbrace != 0 ? $nl_after_rbrace / $rbrace  : "null");


## count paren and space
my $tab_space = "  ";  #size 2

my $lparen = 0;
my $num_space_before_lparen = 0;
my $num_space_after_lparen = 0;

# space before (
while( $content =~ m/(?<before>[ \t]*)\(/sg )
{
	$lparen++;

	my $before = $+{before};
	$before =~ s/\t/$tab_space/g;
	$num_space_before_lparen += length $before;
}

# space after (
while( $content =~ m/\((?<after>[ \t]*)/sg )
{
	my $after  = $+{after};
	$after =~ s/\t/$tab_space/g;
	$num_space_after_lparen  += length $after;
}


my $rparen = 0;
my $num_space_before_rparen = 0;
my $num_space_after_rparen = 0;

# space before )
while( $content =~ m/(?<before>[ \t]*)\)/sg )
{
	$rparen++;

	my $before = $+{before};
	$before =~ s/\t/$tab_space/g;
	$num_space_before_rparen += length $before;
}

# space after )
while( $content =~ m/\)(?<after>[ \t]*)/sg )
{
	my $after  = $+{after};
	$after =~ s/\t/$tab_space/g;
	$num_space_after_rparen  += length $after;
}

say ($lparen != 0 ? $num_space_before_lparen / $lparen : "null");
say ($lparen != 0 ? $num_space_after_lparen / $lparen  : "null");
say ($rparen != 0 ? $num_space_before_rparen / $rparen : "null");
say ($rparen != 0 ? $num_space_after_rparen / $rparen  : "null");


# if (
my @nums_space_before_if_lparen;
my @nums_space_after_if_lparen;

while( $content =~ m/if(?<spBfIf>\s*)\((?<spAfIf>\s*)/sg )
{
	my $after  = $+{spAfIf};
	my $before = $+{spBfIf};
	$after  =~ s/\t/$tab_space/g;
	$before =~ s/\t/$tab_space/g;

	push @nums_space_before_if_lparen, length $before;
	push @nums_space_after_if_lparen,  length $after;
}

say mean @nums_space_before_if_lparen;
say mean @nums_space_after_if_lparen;


# for (
my @nums_space_before_for_lparen;
my @nums_space_after_for_lparen;

while( $content =~ m/for(?<spBfFor>\s*)\((?<spAfFor>\s*)/sg )
{
	my $after  = $+{spAfFor};
	my $before = $+{spBfFor};
	$after  =~ s/\t/$tab_space/g;
	$before =~ s/\t/$tab_space/g;

	push @nums_space_before_for_lparen, length $before;
	push @nums_space_after_for_lparen,  length $after;
}

say mean @nums_space_before_for_lparen;
say mean @nums_space_after_for_lparen;


# while (
my @nums_space_before_while_lparen;
my @nums_space_after_while_lparen;

while( $content =~ m/while(?<spBfWhile>\s*)\((?<spAfWhile>\s*)/sg )
{
	my $after  = $+{spAfWhile};
	my $before = $+{spBfWhile};
	$after  =~ s/\t/$tab_space/g;
	$before =~ s/\t/$tab_space/g;

	push @nums_space_before_while_lparen, length $before;
	push @nums_space_after_while_lparen,  length $after;
}

say mean @nums_space_before_while_lparen;
say mean @nums_space_after_while_lparen;


# operator (
my @nums_space_before_operator_lparen;
my @nums_space_after_operator_lparen;

while( $content =~ m/[\+\-\*\/%](?<spBfOp>\s*)\((?<spAfOp>\s*)/sg )
{
	my $after  = $+{spAfOp};
	my $before = $+{spBfOp};
	$after  =~ s/\t/$tab_space/g;
	$before =~ s/\t/$tab_space/g;

	push @nums_space_before_operator_lparen, length $before;
	push @nums_space_after_operator_lparen,  length $after;
}

say 
	(
	  $#nums_space_before_operator_lparen != -1 ?
	  mean @nums_space_before_operator_lparen :
	  "null"
	);
say 
	(
	  $#nums_space_before_operator_lparen != -1 ?
	 mean @nums_space_after_operator_lparen :
	  "null"
	);

# extract variable definition
my @var_lengths;
my $lower_count = 0;
my $upper_count = 0;
my $lower_upper_count = 0;
my $num_count = 0;
my $under_count = 0;

while( $content =~ m/(
(?:static\s+)?
(?:struct\s+)?
(?<type>[\w_][\w\d_]*)\**
	\s+
	\**(?<name>[\w_][\w\d_]*)
	\s*
	(?:\[.*\])*
	\s*
	(?:=\s*.*?)?
	;
)/xg )
{
	if($+{type} ne "return")
	{
		my $name = $+{name};
		push @var_lengths, length $+{name};
		if   ($name =~ /^([a-z]+)$/)  { $lower_count++; }
		elsif($name =~ /^[A-Z]+$/)    { $upper_count++; }

		$lower_upper_count++  if($name =~ /[a-z]/ and $name =~ /[A-Z]/) ;
		$num_count++          if($name =~ /\d/);
		$under_count++        if($name =~ /_/);
	}
}

say mean @var_lengths;
say variance @var_lengths;
  
say $lower_count;
say $upper_count;
say $lower_upper_count;
say $num_count;
say $under_count;

#### logical operation
# before && ||
my @num_space_before_logical_op = ();
while($content =~ /([ ]*)(?:&&|\|\|)/mg)
{
	push @num_space_before_logical_op, length $1;
}

# after && ||
my @num_space_after_logical_op = ();
while($content =~ /(?:&&|\|\|)([ ]*)/mg)
{
	push @num_space_after_logical_op, length $1;
}

say mean @num_space_before_logical_op;
say mean @num_space_after_logical_op;



# before < <= > >= == !=
my @num_space_before_comparison_op = ();
while($content =~ /([ ]*)(?:<|<=|>|>=|==|!=)/mg)
{
	push @num_space_before_comparison_op, length $1;
}

# after < <= > >= == !=
my @num_space_after_comparison_op = ();
while($content =~ /(?:<|<=|>|>=|==|!=)([ ]*)/mg)
{
	push @num_space_after_comparison_op, length $1;
}

say mean @num_space_before_comparison_op;
say mean @num_space_after_comparison_op;

# before = += -= *= /= %= <<= >>= &= |= ^=
my @num_space_before_assign_op = ();
while($content =~ /[^\+\-\*\/%<>&\|\^!=]([ ]*)(?:=|\+=|-=|\*=|\/=|%=|<<=|>>=|&=|\|=|\^=)[^=]/g)
{
	push @num_space_before_assign_op, length $1;
}

# after = += -= *= /= %= <<= >>= &= |= ^=
my @num_space_after_assign_op = ();
while($content =~ /(?:[^\+\-\*\/%<>&\|\^!=]=|\+=|-=|\*=|\/=|%=|<<=|>>=|&=|\|=|\^=)([ ]*)[^=]/g)
{
	push @num_space_after_assign_op, length $1;
}

say mean @num_space_before_assign_op;
say mean @num_space_after_assign_op;


# indent size check
my @indent_sizes = ();
while( $content =~ /^\S.*\n?{\n(?<indent>\s*)/mg )
{
	my $indent = $+{indent};
	my $count = 0;  $count++ while $indent =~ / /g;
	push @indent_sizes, $count if $count != 0;
}

say 
	( ($#indent_sizes != -1) ? mode @indent_sizes : "null" );


#tab or space
if( $#indent_sizes != -1 )
{
	my $indent_size = mode @indent_sizes;
	
	my $indent_count = 0;
	$indent_count += ((length $1) / $indent_size) while($content =~ /^((?: {$indent_size})+)/mg);

	my $tab_count = 0;
	$tab_count += (length $1) while($content =~ /^(\t+)/mg);

	if($indent_count + $tab_count == 0) { say "null"; }
	else                                { say ($indent_count / ($indent_count + $tab_count)); }
}
else
{
	say 0;
}


#### extract function definition
my @nums_func_line = ();

while($content =~ m/
\n
\s*?
(
  (?<type>[\w_][\w\d\n_\s*\[\]]*?)   (?<spAfType>\s+)
  (?<name>[\w_][\w\d_*]*?)           (?<spAfName>\s*)
  (?<args>\( [^(){}]* \))            (?<spBfBody>\s*)
  (?<body> { # begin function
	  (?:
	    (?>[^{}]+)
	    |
	    (?8)
	  )*
  } ) # end function
)
/sgx
)
{
	my $res = $1;
	my $line_count = 1;
	$line_count++ while $res =~ /\n/g;
	push @nums_func_line, $line_count;
}

say mean @nums_func_line;
say variance @nums_func_line;
