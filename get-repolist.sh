#!/usr/bin/sh

for day in `seq -w 1 31`
  do
	for numPage in `seq 6 9`
	do
		curl -u "yuzu16:061475eface3fc480c2487cd538ac22ddc81b25d" \
			 "https://api.github.com/search/repositories?q=created%3A2015-10-${day}+language:c&per_page=100&page=${numPage}" \
              -o "c-repo10${day}-p${numPage}.json"
	  sleep 2s
  done
done

