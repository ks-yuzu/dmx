#! /bin/sh

wdpath="/home/yuzu/works/dmx/wd"

for file in `ls -a data/csv | grep csv | sed -e 's/.csv//'`
do
    cd $wdpath
	
    mkdir repos/$file
	echo "mkdir repos/$file"
	cd repos/$file

	for line in `seq 1 100`
	do
		echo "start cloning $file line $line ..."
		fullpath=`cat ${wdpath}/data/csv/${file}.csv | \
                    sed -n "${line},${line}p" | \
                    awk 'BEGIN {FS=",";} {print $1}' | \
                    sed 's/"//g'`
		git clone https://github.com/${fullpath}.git
	done
done
