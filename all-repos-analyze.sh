#!/bin/sh

mkdir results
for dir in `ls repos`
do
	mkdir results/$dir
	for repo in `ls repos/$dir`
	do
		echo "processing" repos/$dir/$repo
		mkdir results/$dir/$repo
		cat repolist/csv/$dir.csv | grep /$repo\" | sed -n '1,1p' \
			> results/$dir/$repo/my_repoinfo.csv

		for file in `find repos/$dir/$repo | grep -v "\.git" | grep -E "\.c$"`
		do
			./analyze-notitle.pl "$file" | \
		 		paste -s -d ,\
				> results/$dir/$repo/`basename $file`.csv
		done
	done
done

#		 		awk 'BEGIN {FS=": "} {print $2}' | \
