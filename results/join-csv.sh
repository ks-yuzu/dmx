#!/bin/sh

for dir in `ls`
do
  for repo in `ls $dir`
  do
    for csv in `ls "$dir/$repo" | grep -v ^my_repoinfo.csv$`  #cat `find $dir/$repo | grep .csv`
	do
	  cat $dir/$repo/$csv $dir/$repo/my_repoinfo.csv | tr '\n' ','
	  echo ""
	done
  done
done
